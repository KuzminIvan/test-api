<?php
namespace backend\controllers;
use yii\filters\auth\HttpBasicAuth;

use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;

/**
 * Site controller
 */
class TaskController extends ActiveController
{
    public $modelClass = 'backend\models\Task';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBasicAuth::className(),
        ];
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        if (in_array($action, ['delete'])) {
            throw  new ForbiddenHttpException('Forbidden.');
        }
    }

}

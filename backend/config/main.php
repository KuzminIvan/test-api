<?php

use yii\web\Response;
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                //'application/xml' => 'yii\web\XmlParser',
            ],
            'enableCookieValidation' => false
        ],
        'response' => [
            'format' => Response::FORMAT_JSON,
            'formatters' => [
                'json' => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG,
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
            'enableSession' => false,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => [
            'class' => '\yii\db\Connection',
            'dsn' => 'mysql:host=mysql; dbname=test_api',
            'username' => 'admin',
            'password' => '123123',
            'charset' => 'utf8'
        ],
//        'errorHandler' => [
//            'errorAction' => 'site/error',
//        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules'=>[
                //'' => 'site/index',
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'task',
                ]
            ]
        ],
    ],
    'params' => $params,
];

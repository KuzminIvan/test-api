1)  Please create directory:
mkdir mysql-db;
sudo chmod -R 0777 mysql-db;

2) Run docker-compose
docker-compose up -d --build

3) Task list:
GET http://test-back.local.com:8080/tasks
Authorization: Basic WGVBcm1pU3VJUktzQVk1R2ZUdnhCejE2RERmUEhZWEs6 

POST - create task
body
{
    "text": "test",
    "priority": 2,
    "status": 0
}
PUT, PATCH - update task
DELETE - forbidden (just to demonstrate authorization)
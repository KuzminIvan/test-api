<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->null(),
            'password_hash' => $this->string()->null(),
            'access_token'=>$this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
        ], $tableOptions);

        $this->insert('{{%user}}',[
            'username'=>'test',
            'access_token'=>'XeArmiSuIRKsAY5GfTvxBz16DDfPHYXK'
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

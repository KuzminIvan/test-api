<?php

use yii\db\Migration;

/**
 * Class m201130_135443_create_task
 */
class m201130_135443_create_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%task}}',[
            'id'=>'pk',
            'text'=>'VARCHAR(255) NOT NULL',
            'priority'=>'TINYINT DEFAULT 1',
            'status'=>'TINYINT(1) DEFAULT 0'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropTable('{{%task}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201130_135443_create_task cannot be reverted.\n";

        return false;
    }
    */
}
